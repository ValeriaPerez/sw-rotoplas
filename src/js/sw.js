self.addEventListener("install", function(event){
  console.log("Instalando SW");
  event.waitUntil(
    caches.open("simple_sw").then(function(cache){
      console.log(" SW Instalado.")
      console.log("off and refresh")
      cache.addAll([
        "rotoplas-ahorro-a.html",
        "static/css/style.css",
        "static/js/jquery-2.1.4.min.js",
        "static/js/forms.js",
        "static/js/formValidator.js",
        "static/img/logo-White.png",
        "static/img/arriba_curvas_azul.png",
        "static/img/cocina.jpg",
        "static/img/abajo_curvas_azul.png",
        "static/img/abajo_curvas_verde.png",
        "static/img/ahorro.svg",
        "static/img/cancela.svg",
        "static/img/cartucho.svg",
        "static/img/iconoEco.svg",
        "static/img/mant.svg",
        "static/img/pago.svg",
        "static/img/reduce.svg",
        "static/img/fondoAzul.png"
      ])
    })
  )
})

/***** Service Worker Fetch ****/

self.addEventListener("fetch", function(event){
  event.respondWith(
    fetch(event.request).catch(function() {
      console.log("Fetch");
      return caches.match(event.request);
    })
  )
})


