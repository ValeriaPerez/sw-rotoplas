<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
/* vars for export */
// database record to be exported
$db_record = 'leads';
// optional where query
$where = '';
// filename for export
$csv_filename = 'leads_'.date('Y-m-d').'.csv';
$mysqli = mysqli_connect("localhost", "rotoplas", "R0t0pl4s8364", "rotoplas");
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

// create empty variable to be filled with export data
$csv_export = '';
// query to get data from database
$query = mysqli_query($mysqli, "SELECT * FROM ".$db_record." ".$where);


while ($property = mysqli_fetch_field($query)) {
    $csv_export .=  $property->name.';';
    $fields[]= $property->name;
}
// newline (seems to work both on Linux & Windows servers)
$csv_export.= '
';
// loop through database query and fill export variable
while($row = mysqli_fetch_array($query)) {
	foreach ($fields as $key) {
		$csv_export.= '"'.$row[$key].'";';
	}
  $csv_export.= '
';	
}
// Export the data and prompt a csv file for download
header("Content-type: text/x-csv");
header("Content-Disposition: attachment; filename=".$csv_filename."");
echo($csv_export);
?>
