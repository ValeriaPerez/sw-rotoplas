var ActionUrl = function(){
	function assignParams() {
		var $_GET = {};
		document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
		function decode(s) {
			return decodeURIComponent(s.split("+").join(" "));
		}
		$_GET[decode(arguments[1])] = decode(arguments[2]); });
		//console.log($_GET);

		$_GET.hasOwnProperty('utm_source') ? $(".utm_lead_source").val($_GET.utm_source) : $(".utm_lead_source").val(0);
		$_GET.hasOwnProperty('utm_medium') ? $(".utm_lead_medium").val($_GET.utm_medium) : $(".utm_lead_medium").val(0) ;
		$_GET.hasOwnProperty('utm_campaign') ? $(".utm_campaign").val($_GET.utm_campaign) : $(".utm_campaign").val(0) ;
		$_GET.hasOwnProperty('utm_content') ? $(".utm_campaign_content").val($_GET.utm_content) : $(".utm_campaign_content").val(0) ;
		$_GET.hasOwnProperty('utm_term') ? $(".utm_campaign_term").val($_GET.utm_term) : $(".utm_campaign_term").val(0) ;
	}
	return{
		assignParams:assignParams
	}
}();

var ValidateForms = function(){
	function valForms() {
		$(".btnSend").click(function (e) {
			e.preventDefault();

			var btnSend = $(this);
			var isValidform = validarForma(btnSend);
			var formCont = btnSend.closest("form");

			if(isValidform){
				console.log("Datos Validados");
				btnSend.addClass('buttonSend--disabled');
				btnSend.prop('disabled', true);
				// var landing_page = formCont.find(".landing_page");
				// landing_page.val(window.location.href);

				var loader = $("#loader").find(".loader").clone();
				formCont.append(loader);

				var formMessage = formCont.find(".backResponse");
				formMessage.html('');

				var $form = formCont,
				    url = '../../templates/send-data.php';
				$.ajax({
				  method: "POST",
				  url: url,
				  data: $form.serialize()
				})
				.done(function( data ) {
					btnSend.removeClass('buttonSend--disabled');
					btnSend.prop('disabled', false);
					loader.remove();
					loader = "";
				  //console.log( "Data Saved: ");
					var response = JSON.parse(data.trim());
					if(response.success){
						var landing =document.getElementsByClassName("landing_page")[0].value;

						switch(landing) {
						    case 'rotoplas-ahorro-a':
						    case 'rotoplas-ahorro-b':
						        window.location = 'thankyou-ahorro.html';
						        break;
						    case 'rotoplas-calidad-a':
						    case 'rotoplas-calidad-b':
						        window.location = 'thankyou-calidad.html';
						        break;
						    case 'rotoplas-ecologico-a':
						    case 'rotoplas-ecologico-b':
						        window.location = 'thankyou-ecologico.html';
						        break;
						    default:
						        window.location = 'thankyou_rotoplas.html';
						}
					}
					else{
						formMessage.html('Lo sentimos, tuvimos un problema, inténtalo más tarde.');
					}
				});

				return true;
			}
			else{
				errorLabel(formCont);
			}
		});
	}

	function errorLabel(forma){
		forma.find(".error").each(function(){
			if(typeof($(this).data("error")) != "undefined"){
				var errorLabel = $(this).parent().find(".label-error");
				errorLabel.text($(this).data("error"));
			}
		});
	}

	function onlyNumbers(){
		soloNumeros();
	}

	return{
		forms:valForms,
		onlyNumbers:onlyNumbers
	}
}();

$(function(){
	ActionUrl.assignParams();
	ValidateForms.forms();
	ValidateForms.onlyNumbers();
});
