/**
 * Load NPM dependencies
*/
var gulp = require('gulp');
var sass = require('gulp-sass');
var gutil = require("gulp-util");
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync');
var swig = require('gulp-swig');
var plumber = require('gulp-plumber');
//var image = require('gulp-image');
var ugly = require('gulp-uglify');

/**
 * Static server
 */
gulp.task('server', ['templates', 'sass'], function(){
	// Init http server
	browserSync.init({
			server: {
					baseDir: [
						'build/',
						'build/static/',
						'build/templates/',
						'build/static/'
					]
			}
	});
	gulp.watch('src/templates/**/*.html', ['templates']);
	gulp.watch('src/templates/**/*.swig', ['templates']);

	gulp.watch('src/sass/**/*.scss', ['sass']);
});

/**
 * Sass
*/
gulp.task('sass', function () {
	return gulp.src('src/sass/style.scss')
				.pipe(sourcemaps.write())
				// .pipe(sass({outputStyle: 'compressed'})) //para comprimir el css
				.pipe(sass().on('error', function (err) {
            gutil.log( gutil.colors.red('ERROR', 'Compiling Sass Error'));
            gutil.log( gutil.colors.red(err.message));
            gutil.beep();
            browserSync.notify("<span style='color: red;'>Compiling Sass Error<span/>");
            this.emit('end');
        }))
				.pipe(gulp.dest('build/static/css/'))
				.pipe(browserSync.stream({once: true}));
});

/**
 * Swig pages render
 */
gulp.task('templates', function() {
  return gulp.src('src/templates/**/*.html')
        .pipe(
            swig({ defaults: { cache: false } })
        )
        .pipe(plumber())
        .pipe(gulp.dest('build/'))
        .pipe(browserSync.stream({once: true}));
});

/**
 * Image compressed
 */
gulp.task('image', function () {
  return gulp.src('src/img/*')
    		.pipe(image())
    		.pipe(gulp.dest('build/static/img'));
});

/**
 * Image compressed
 */
gulp.task('ugly', function(){
	return gulp.src('src/js/*.js')
		//.pipe(ugly())
		.pipe(gulp.dest('build/static/js'))
});


gulp.task('live', ['server']);
