var express = require('express');


var app = express();
var host = '0.0.0.0',
    port = 3000;

var server = app.listen(port, host, function(){
  console.log('Listening at http://%s:%s', host, port);
  console.log('Index -  http://%s:%s/rotoplas-ahorro-a.html', host, port);
});

app.use(express.static('build'))

app.get('/rotoplas-ahorro-a.html', function(req, res){
  res.send('rotoplas-ahorro-a.html');
});
